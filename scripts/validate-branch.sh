#!/bin/bash

IFS='/'
read -a splitted <<< "$CI_COMMIT_BRANCH"

validate_prefix() {
  suffix="${splitted[0]}"

  if [[ $suffix = feature || $suffix = fix ]]
    then
      echo Branch suffix is named properly
    else
      echo Branch name should start with one of the [\"feature\", \"fix\"] and be followed by \"/\"
      exit 1
  fi
}

validate_suffix() {
  prefix="${splitted[1]}"

  if [[ $prefix =~ [^a-z_] ]]
    then
      echo Prefix should contain only a-z \(lowercase\) and/or underscore
      exit 1
    else
      echo Branch prefix is named properly
 fi
}

validate_prefix

validate_suffix


