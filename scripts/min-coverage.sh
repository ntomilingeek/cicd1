#!/bin/bash

report=$(cat coverage/coverage-summary.json)
pct='(\"pct\":[0-9]+\.*[0-9]*)'


extract_coverage() {
if [[ $report =~ $pct ]];
  then
    percent_row="${BASH_REMATCH[0]}"
  else
    echo Coverage substring is not found by regex, check the 'coverage/coverage-summary.json' file
    exit 1
fi

if [[ $percent_row =~ [0-9][0-9]* ]];
  then
    coverage="${BASH_REMATCH[0]}"
  else
    echo No coverage percent has been found
    exit 1
fi
}

validate_coverage() {
  if [[ $coverage -gt $MIN_COVERAGE || $coverage -eq $MIN_COVERAGE ]];
  then
    echo Coverage is ok
  else
    echo Coverage should be \> 80%
    exit 1
fi
}

extract_coverage
validate_coverage
