module.exports = {
    collectCoverage: true,

    collectCoverageFrom: [
        'src/**/*.js',
    ],
    coverageDirectory: 'coverage',
    coverageReporters: [
        'json-summary',
        // 'html',
        'text-summary'
    ],
    coverageProvider: 'v8',
};
