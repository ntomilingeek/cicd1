const { sumNumbers } = require('./modules/sum');
const app = require('express')();

const PORT = 3000;

app.get('/', (req, res) => {
   res.send({ hello: 'world'} );
})

app.get('/sum', (req, res) => {
    res.send(sumNumbers(+req.query.a, +req.query.b))
})

app.listen(PORT, () => {
    console.log(`Server is listening on port ${ PORT }`);
});
