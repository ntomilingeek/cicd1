const sumNumbers = (a, b) => a + b;

module.exports = {
    sumNumbers,
}
