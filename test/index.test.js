const { sumNumbers } = require('../src/modules/sum');

test('1 + 2 equals 3', () => {
    let b = sumNumbers(1, 2);
    expect(b).toBe(3);
})
